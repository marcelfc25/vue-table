export default {
	ALTER_ITENS: (state, itens) => {
		state.itens = itens
	},
	ALTER_NUMBER_OF_PAGES: (state, numberOfPages) => {
		state.numberOfPages = numberOfPages
	}
}